@extends('layouts.app')

@section('head')
<script src="{{ asset('js/bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
@auth
    <div class="content-container">
        <div class="toolbar flex-row pa-1 relative">
            <div flex py-1>Nutslack</div>
            <div class="name pa-1">{{ Auth::user()->name }}</div>
            <a class="btn btn-primary" href="{{ route('logout') }}">Logout</a>
        </div>

        <div class="messages-container flex-column">
            @foreach($messages as $message)
                <div class="message-wrapper {{ Auth::user()->id == $message->user->id ? 'me' : '' }}">
                    <div class="message">
                        <div class="text">{{ $message->message }}</div>

                        <div class="sender">
                            {{ Auth::user()->id == $message->user->id ? 'you' : $message->user->name }}
                        </div>
                    </div>
                    <div class="sent" data-sent="{{ $message->created_at }}">
                        {{ $message->created_at->diffForHumans() }}
                    </div>
                </div>
            @endforeach
        </div>
        <template id="newMessage">
            <div class="message-wrapper">
                <div class="message">
                    <div class="text">{{ $message->message }}</div>

                    <div class="sender">
                        {{ Auth::user()->id == $message->user->id ? 'you' : $message->user->name }}
                    </div>
                </div>
                <div class="sent">
                    {{ $message->created_at->diffForHumans() }}
                </div>
            </div>
        </template>
        <div class="input-container">
            <input id="message" type="text" class="form-control chat-input" name="message" required autofocus>
        </div>
    </div>
@else
    <div class="flex-center relative full-height">
        <div class="content">
            <div class="title m-b-md">Nutslack</div>

            <div class="links">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">Name</label>
                        <div>
                            <input id="name" type="text" class="form-control" name="name"
                                value="{{ old('name') }}" required autofocus autocomplete="name">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">Password</label>
                        <div>
                            <input id="password" type="password" class="form-control" name="password"
                                required autocomplete="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div flex-row flex flex-end my-1>
                        <button type="submit" class="btn btn-primary">Lemme in!</button>
                    </div>
                    <!-- <button type="submit" class="btn btn-primary float-right">Lemme in!</button> -->
                </form>
                <div flex-row space-between>
                    <div flex-center>Don't have an account?</div>
                    <a class="btn btn-primary" href="{{ route('register') }}">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

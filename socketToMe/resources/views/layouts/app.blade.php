<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Nutslack</title>

    <link href="{{ asset('css/base.css') }}" rel="stylesheet" type="text/css">
    @yield('head')
</head>
<body>
    @yield('content')
</body>
</html>

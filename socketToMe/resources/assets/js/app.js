document.addEventListener('DOMContentLoaded', () => {
    const me = document.querySelector('.name').innerText;

    const newMessage = data => {
        const isMe = me === data.user.name;
        const template = `
            <div class="message">
                <div class="text">${data.message.message}</div>
                <div class="sender">${isMe ? 'you' : data.user.name}</div>
            </div>
            <div class="sent" data-sent="${data.created_at}">${moment.utc(data.created_at).fromNow()}</div>
        `;

        const chat = document.querySelector('.messages-container');
        if (chat) {
            const div = document.createElement('div');
            div.className = `message-wrapper${isMe ? ' me' : ''}`;
            div.innerHTML = template;
            chat.appendChild(div);
            container.scrollTo(0, container.scrollHeight);
        }
    }

    window.Echo.channel('chat').listen('MessageSent', e => newMessage(e));

    const container = document.querySelector('.messages-container');
    if (container) {
        container.scrollTo(0, container.scrollHeight);
    }

    const messageInput = document.querySelector('#message');
    messageInput.addEventListener('keydown', e => {
        if (e.keyCode === 13) {
            e.preventDefault();
            const message = messageInput.value;
            if (message) {
                messageInput.value = '';
                axios.post('messages', { message })
                    .then(res => newMessage(res.data.message))  // inject
                    .catch(err => console.log(err));
            }
        }
    });

    setInterval(() => {
        document.querySelectorAll('.sent').forEach(s => {
            const data = s.getAttribute('data-sent');
            console.log(data);
            s.innerText = moment.utc(data).fromNow();
        });
    }, 30000);
});

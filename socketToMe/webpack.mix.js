const mix = require('laravel-mix');
const fs = require('fs');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

const assetRoot = `${__dirname}/resources/assets/`;

// Read and mix sass
fs.readdirSync(`${assetRoot}/sass`)
    .filter(f => /^[^_].*\.scss$/.test(f))  // Ignore non-scss files / folders
    .forEach(file => mix.sass(`resources/assets/sass/${file}`, 'public/css'))

// Read and mix js
fs.readdirSync(`${assetRoot}/js`)
    .filter(f => /^[^_].*\.js$/.test(f))  // Ignore non-js files / folders
    .forEach(file => mix.js(`resources/assets/js/${file}`, 'public/js'));

// .concat(fs.readdirSync(`${assetRoot}/js`))
// inputFiles.forEach(file => {
//     if (/\.js$/.test(file)) {
//         mix.js(`resources/assets/js/${file}`, 'public/js');
//     } else {
//         mix.js(`resources/assets/sass/${file}`, 'public/css');
//     }
// });


<?php

//use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'MessagesController@view')->name('app');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/messages', 'MessagesController@index');
Route::post('/messages', 'MessagesController@create');

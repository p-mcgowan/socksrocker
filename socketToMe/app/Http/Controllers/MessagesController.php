<?php

namespace App\Http\Controllers;

use App\Message;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\View;

class MessagesController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('app', ['messages' => $this->index()]);
    }

    /**
     * Fetch all messages
     *
     * @return Message[]
     */
    public function index()
    {
        return Message::with('user')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message')
        ]);

        broadcast(new MessageSent($user, $message))->toOthers();

        return [
            'message' => [
                'message' => $message,
                'user' => $user
            ],
            'status' => 'Message sent'
        ];
    }
}
